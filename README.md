# Green Templeton College Coding for Medical Students Series

This is the repository of materials for this seminar series organised by the Richard Doll Society. Materials will be added as we progress, and will remain freely available here.

## Why medical students should learn to code
Why coding is a useful skill, what kinds of things you could do, and how to get started.
Slides [here](00_intro_slides.pdf).

## Introduction to python programming
A simple introduction to basic python concepts. 

You can either run the [online interactive notebook](https://colab.research.google.com/drive/1do0umh2PiFxk99vbCNOBjWUHbhZ8MCGk) or download the [Notebook file](02_analysis_pandas.ipynb) and run locally using [Jupyter](https://jupyter.org/).

[How to use Jupyter notebooks](https://www.codecademy.com/articles/how-to-use-jupyter-notebooks)

## Data analysis and visualisation using python toolkits
A demonstration of the flexibility and power of python data analysis and plotting toolkits.

You can either run the [online interactive notebook](https://colab.research.google.com/drive/1Yj-DPTOUoX24PhHRKf63va2JKLs85RQF) or download the [Notebook file](02_analysis_pandas.ipynb) and run locally using [Jupyter](https://jupyter.org/).

## Academic writing using $`\LaTeX`$
Provisional

## Machine learning taster
Provisional


## Other useful resources

### Introduction to the shell
Following the tutorial at [http://swcarpentry.github.io/shell-novice](http://swcarpentry.github.io/shell-novice/)

![alt text][logo]

[logo]: img/gtc_logo.jpeg "GTC logo"

